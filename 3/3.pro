TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += /usr/local/systemc-2.3.1/include
LIBS += -L/usr/local/systemc-2.3.1/lib-linux64/ -lsystemc

SOURCES += main.cpp \
    Types.cpp \
    qdbmp.cpp \
    ImSink.cpp

HEADERS += \
    Types.h \
    qdbmp.h \
    MedFilter.h \
    ImSource.h \
    ImSink.h \
    FIRFilter.h \
    Filter.h \
    AvgFilter.h

