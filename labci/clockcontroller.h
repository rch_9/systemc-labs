#ifndef CLOCKCONTROLLER
#define CLOCKCONTROLLER

#include <systemc.h>
/*
: bool setup;
Выходные сигналы: sc_uint<5> hours; sc_uint<6> minutes;
*/

SC_MODULE(ClockController)
{
    sc_in<bool> clk;
    sc_in<bool> setup;

    sc_out<sc_uint<5>> hours;
    sc_out<sc_uint<6>> minutes;

    sc_signal<sc_uint<6>> seconds;

    void watchTime();

    SC_CTOR(ClockController)
    {
        SC_METHOD(watchTime);

        sensitive << clk;
    }
};


#endif // CLOCKCONTROLLER

