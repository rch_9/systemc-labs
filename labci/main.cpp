#include <systemc.h>
#include "clockcontroller.h"
#include "clockcontrollertest.h"

// main is defined in systemc.lib
int sc_main(int argc,char *argv[])
{
    ClockController clockCont("clockCont");
    ClockControllerTest test("test");

    sc_clock s_clk("clk", 2, SC_SEC);

    sc_signal<bool> s_setup("setup");
    sc_signal<sc_uint<5>> s_hours("hours");
    sc_signal<sc_uint<6>> s_minutes("minutes");
    s_setup.write(1);        

    clockCont.clk(s_clk);
    clockCont.setup(s_setup);
    clockCont.hours(s_hours);
    clockCont.minutes(s_minutes);

    test.clk(s_clk);
    test.minutes(clockCont.minutes);

    sc_trace_file *tf = sc_create_vcd_trace_file("lab_clock");
    tf->set_time_unit(1, SC_SEC);

    sc_trace(tf, s_clk, "clk");
    sc_trace(tf, s_setup, "setup");
    sc_trace(tf, clockCont.hours, "h");
    sc_trace(tf, clockCont.minutes, "m");

    sc_start(15000, SC_SEC);

    sc_close_vcd_trace_file(tf);

    return 0;
}
