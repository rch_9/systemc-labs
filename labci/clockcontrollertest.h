#ifndef CLOCKCONTROLLERTEST
#define CLOCKCONTROLLERTEST

#include <systemc.h>

SC_MODULE(ClockControllerTest)
{
    sc_in<bool> clk;       
    sc_in<sc_uint<6>> minutes;

    //void generate_setup();
    void checkMinutes();

    SC_CTOR(ClockControllerTest)
    {
        SC_METHOD(checkMinutes);
        sensitive << clk;
    }
};

#endif // CLOCKCONTROLLERTEST

