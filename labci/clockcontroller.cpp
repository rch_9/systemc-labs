#include "clockcontroller.h"

void ClockController::watchTime()
{
    if(setup.read())
    {
        seconds.write(seconds.read() + 1);

        if(seconds.read() == 59)
        {
            seconds.write(0);
            minutes.write(minutes.read() + 1);
            if(minutes.read()  == 59)
            {
                hours.write(hours.read() + 1);
                minutes = 0;
            }
        }
    }
}
