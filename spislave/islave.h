#ifndef ISLAVE
#define ISLAVE


#include <systemc.h>


/**
 Impulse generator:
 creates <count> impulses every <sync>
 */
SC_MODULE(ISlave)
{
    /*** INPUT SIGNALS: ***/
    sc_in<sc_uint<8>> mosi;
    sc_in<bool> ss_n;
    sc_in<bool> sclk;

    /*** OUTPUT SIGNALS: ***/
    sc_out<bool> miso;

    /*** INTERNAL SIGNALS: ***/
    //sc_signal<sc_uint<4> > count_left;

    void take();

    SC_CTOR(ISlave)
    {
        SC_METHOD(take);
        sensitive << clk.pos();
    }
};


#endif // ISLAVE

