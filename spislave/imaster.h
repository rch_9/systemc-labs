#ifndef IMASTER
#define IMASTER

#include <systemc.h>



SC_MODULE(IMaster)
{
    /*** INPUT SIGNALS: ***/    
    sc_in<bool> miso;
    sc_in<bool> cs;
    sc_in<bool> dout_ready;

    /*** OUTPUT SIGNALS: ***/    
    sc_out<bool> mosi;
    sc_out<sc_uint<8>> dout;
    sc_out<bool> dout_valid;




    /*** INTERNAL SIGNALS: ***/
    //sc_signal<sc_uint<4> > count_left;

    void send();

    SC_CTOR(IMaster)
    {
        // on_tick will be called on every positive clk
        SC_METHOD(send);
        //sensitive << sclk.pos();
    }
};


#endif // IMASTER

