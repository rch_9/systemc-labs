TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

INCLUDEPATH += /usr/local/systemc-2.3.1/include
LIBS += -L/usr/local/systemc-2.3.1/lib-linux64/ -lsystemc

SOURCES += main.cpp \
    imaster.cpp \
    islave.cpp

HEADERS += \
    imaster.h \
    islave.h

